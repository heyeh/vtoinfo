# vtoinfo

#### Description

A package that obtains the visitor's IP, City, System, Browser, and other information.

Written and published as ES6 using Ts, without using any third-party libraries and without relying on any frameworks. Ideal for introducing Vue, React, or other Js/Ts front-end frameworks.

#### Software Architecture

This package sends detection information to multiple APIs at the same time, with the fastest return as the result, so it is not only reliable, but also very fast! For this purpose, a PromiseAny function was implemented without Using Promise.any to guarantee ES6 compatibility, rather than having to ES2021.

#### Installation

import {vtoinfo} from 'vtoinfo'

vtoinfo().then ( rec => {

  ...your code

})

Rec is an object that is structured as follows:

 {ip: '112.41.0.110', province: 'Liaon', city: 'shenyang', os: 'win7', browser: 'edge'}

Or, some developers like to write:

import {vtoinfo} from 'vtoinfo'

async function visitor() {

  let info = await vtoinfo()

  ...your code

}

**Note**: This package can also be introduced in the form of commonJS:

const vtoinfo = require('vtoinfo')

**Tip**: The information returned is Chinese, if switching English, please change the ip api site.

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request
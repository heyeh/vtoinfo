declare type jsonInfo = {
    ip: string;
    pro?: string;
    province?: string;
    city: string;
    os?: string;
    browser?: string;
};
export declare function vtoinfo(): Promise<jsonInfo>;
export {};

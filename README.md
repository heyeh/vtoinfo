# vtoinfo

#### 介绍

前端获取访问者ip地址、所在城市、操作系统、浏览器等信息的一个包。

使用Ts编写并发布成ES6，没使用任何第三方库，也不依赖任何框架。非常适合Vue、React或其他Js/Ts前端框架引入使用。

#### 软件架构

这个包同时向多个api发检测信息，以最快返回的作为结果，所以不仅可靠性好，速度也非常快！为此实现了一个PromiseAny函数，而没使用Promise.any，以保证兼容ES6，而不是必须ES2021。

#### 安装教程

npm i vtoinfo --save

或

yarn add vtoinfo

#### 使用说明

import {vtoinfo} from 'vtoinfo'

vtoinfo().then ( rec => {

  ...您的业务代码

})

rec是一个对象，内容如下：

 {ip: '112.41.0.110', province: '辽宁省', city: '沈阳市', os: 'win7', browser: 'edge'}

或者，有的开发者喜欢这样写：

import {vtoinfo} from 'vtoinfo'

async function visitor() {

  let info = await vtoinfo()

  ...您的业务代码

}

注：这个包也可以用commonJS方式引入：

const vtoinfo = require('vtoinfo')

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat 分支
3. 提交代码
4. 新建 Pull Request